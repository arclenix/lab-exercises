#	Mandatory 3: Create a program so it implement Factory Method Pattern. There are a template class to use Factory Method Pattern but you are free to explore your idea in implementing Factory Method Pattern.

from abc import ABCMeta, abstractmethod

def main():
	n = 10
	store = BoardStore()
	board = store.orderBoard(n)
	print(board)

class BoardStore(object):
	def orderBoard(self, n):
		return BoardFactory().createBoard(n)
		
class BoardFactory(object):
	def createBoard(self, n):
		return Boardnxn(n)

class AbstractBoard:
	def __init__(self, rows, columns):
		self.board = [[None for _ in range(columns)] for _ in range(rows)]
		self.populate_board()
		
	def populate_board(self):
		raise NotImplementedError()

	def __str__(self):
		squares = []
		for x, row in enumerate(self.board):
			for y, column in enumerate(self.board):
				squares.append(self.board[x][y])
			squares.append("\n")
		return "".join(squares)

class Boardnxn(AbstractBoard):
	def __init__(self, n):
		self.n = n
		super().__init__(n, n)
		
	def populate_board(self):
		for row in range(self.n):
			for column in range(self.n):
				self.board[row][column] = self.create_piece(row, column)
	
	def create_piece(self, row = 1, column = 1):
		if(row % 2):
			if (column % 2): 			
				Piece = VPiece()
			else: 
				Piece = UPiece()
		else: 
			if (column % 2): 			
				Piece = Circle()
			else: 
				Piece = Cross()
		Piece.setSymbol()
		return Piece.getSymbol()

# Mandatory 3: Uncomment the codes below
#abstract class
class Piece(metaclass = ABCMeta):
	@abstractmethod
	def setSymbol(self):
		pass
	@abstractmethod
	def getSymbol(self):
		pass
	
class Circle(Piece):
# Mandatory 3: Implement the Factory Method in here
	__symbol = ""
	
	def setSymbol(self):
		self.__symbol = "o"
		
	def getSymbol(self):
		return self.__symbol;

class Cross(Piece):
# Mandatory 3: Implement the Factory Method in here
	__symbol = ""
	def setSymbol(self):
		self.__symbol = "x"
		
	def getSymbol(self):
		return self.__symbol;
	
class UPiece(Piece):
	__symbol = ""
	
	def setSymbol(self):
		self.__symbol = "u"
		
	def getSymbol(self):
		return self.__symbol;

		
class VPiece(Piece):
	__symbol = ""
	
	def setSymbol(self):
		self.__symbol = "v"
		
	def getSymbol(self):
		return self.__symbol;

if __name__ == "__main__":
    main()

				
