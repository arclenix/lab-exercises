#	Mandatory 3: Create a program so it implement Factory Method Pattern. There are a template class to use Factory Method Pattern but you are free to explore your idea in implementing Factory Method Pattern.

from abc import ABCMeta, abstractmethod

def main():
	store = BoardStore()
	board = store.orderBoard()
	print(board)

class BoardStore(object):
	def orderBoard(self):
		return BoardFactory().createBoard()
		
class BoardFactory(object):
	def createBoard(self):
		return Board3x3()

class AbstractBoard:
	def __init__(self, rows, columns):
		self.board = [[None for _ in range(columns)] for _ in range(rows)]
		self.populate_board()
		
	def populate_board(self):
		raise NotImplementedError()

	def __str__(self):
		squares = []
		for x, row in enumerate(self.board):
			for y, column in enumerate(self.board):
				squares.append(self.board[x][y])
			squares.append("\n")
		return "".join(squares)

class Board3x3(AbstractBoard):
	def __init__(self):
		super().__init__(3, 3)
		
	def populate_board(self):
		for row in range(3):
			for column in range(3):
				self.board[row][column] = self.create_piece(column)
	
	def create_piece(self, column = 1):
		if (column % 2): 			
			Piece = Circle()
		else: 
			Piece = Cross()
		Piece.setSymbol()
		return Piece.getSymbol()

# Mandatory 3: Uncomment the codes below
#abstract class
class Piece(metaclass = ABCMeta):
	@abstractmethod
	def setSymbol(self):
		pass
	@abstractmethod
	def getSymbol(self):
		pass
	
class Circle(Piece):
# Mandatory 3: Implement the Factory Method in here
	__symbol = ""
	
	def setSymbol(self):
		self.__symbol = "o"
		
	def getSymbol(self):
		return self.__symbol;

class Cross(Piece):
# Mandatory 3: Implement the Factory Method in here
	__symbol = ""
	def setSymbol(self):
		self.__symbol = "x"
		
	def getSymbol(self):
		return self.__symbol;

if __name__ == "__main__":
    main()
				
