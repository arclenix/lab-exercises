"""Credit to Eli Bendersky"""

import multiprocessing as mp
import math


def factorize_naive(n):
    """ A naive factorization method. Take integer 'n', return list of
        factors.
    """
    if n < 2:
        return []
    factors = []
    p = 2

    while True:
        if n == 1:
            return factors

        r = n % p
        if r == 0:
            factors.append(p)
            n = n // p
        elif p * p >= n:
            factors.append(n)
            return factors
        elif p > 2:
            # Advance in steps of 2 over odd numbers
            p += 2
        else:
            # If p == 2, get to 3
            p += 1
    assert False, "unreachable"

def pool_factorizer_chunked(nums, nprocs):
    # Manually divide the task to chunks of equal length, submitting each
    # chunk to the pool.
    chunksize = int(math.ceil(len(nums) / float(nprocs)))
    futures = []

    with ProcessPoolExecutor() as executor:
        for i in range(nprocs):
            chunk = nums[(chunksize * i): (chunksize * (i + 1))]
            futures.append(executor.submit(chunked_worker, chunk))

    resultdict = {}
    for f in as_completed(futures):
        resultdict.update(f.result())
    return resultdict

def create_processes(jobs, result, concurrency): 
    for _ in range(concurrency):
        process = mp.Process(target = worker, args = (jobs, result))
        process.daemon = True
        process.start()

def worker(jobs, result):
    while True:
        try:
            num = jobs.get()
            result.put({num : factorize_naive(num)})
        finally:
            jobs.task_done()
            
def add_jobs(jobs, nums):
    for n in nums:
        jobs.put(n)

if __name__ == '__main__':
    nums = [25, 36, 42, 88, 99]
    jobs = mp.JoinableQueue()
    result = mp.Queue()
    concurrency = 2
    create_processes(jobs, result, concurrency)
    add_jobs(jobs, nums)
    jobs.join()
    
    resultdict = {}
    while not result.empty():
        resultdict.update(result.get_nowait())
    print(resultdict)
