from collections import namedtuple
from concurrent.futures import ProcessPoolExecutor, as_completed
import multiprocessing as mp
import random
import time

VALUES = (100, 200, 500, 1000)
Coin = namedtuple('Coin', ['value'])


def reader(dictionary, coin):
    if mp.current_process().name in dictionary:
        dictionary[mp.current_process().name] = dictionary[mp.current_process().name] + coin.value
    else:
        dictionary[mp.current_process().name] = coin.value
    time.sleep(random.random() * 0.50)
    print("[Process {}] Read coin ({})".format(reader_name(mp.current_process().name), str(coin.value)))
    
def reader_name(process_name):
    return "Reader " + str(int(process_name.split("-")[1]) - 1)

def writer(count, dictionary, executor):
    writer_sum = 0

    for ii in range(count):
        coin = Coin(random.choice(VALUES))
        executor.submit(reader, dictionary, coin)
        writer_sum += coin.value

        # No need to prepend string with process name since this
        # function is executed in main interpreter thread
        print("Put coin ({})".format(coin.value))
        time.sleep(random.random() * 0.50)
        
    print('Total value written: ' + str(writer_sum))


if __name__ == '__main__':
    start_time = time.time()
    count = 100
    manager = mp.Manager()
    dictionary = manager.dict()

    with ProcessPoolExecutor(max_workers = 2) as executor:
        writer(count, dictionary, executor)
    
    for key in dictionary.keys():
        print("[Process {}] Total value read: {}".format(reader_name(key), dictionary[key]))
   
    end_time = time.time()
    print('Total running time: ' + str(end_time - start_time))
