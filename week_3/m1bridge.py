class Shape(object):
	def __init__(self, drawAPI):
		self.drawAPI = drawAPI
	
	def draw(self):
		pass
		
class Circle(Shape):
	def __init__(self, x, y, radius, drawAPI):
		super().__init__(drawAPI)
		self.x = x
		self.y = y
		self.radius = radius
	
	def draw(self):
		self.drawAPI.drawCircle(self.radius, self.x, self.y)
			
class DrawAPI(object):
	def drawCircle(self, radius, x, y):
		pass
		
class RedCircle(DrawAPI):
	def drawCircle(self, radius, x, y):
		print("Drawing Circle[ color: red, radius: {}, x: {}, y: {}]".format(radius, x, y))

class GreenCircle(DrawAPI):
	def drawCircle(self, radius, x, y):
		print("Drawing Circle[ color: green, radius: {}, x: {}, y: {}]".format(radius, x, y))

		
def main():
	redCircle = Circle(100, 100, 10, RedCircle())
	greenCircle = Circle(100, 100, 10, GreenCircle())
	
	redCircle.draw()
	greenCircle.draw()

if __name__ == "__main__":
    main()