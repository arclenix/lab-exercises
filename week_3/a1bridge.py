class UseCase(object):
	def __init__(self, operatingSystemAPI):
		self.operatingSystemAPI = operatingSystemAPI
	
	def someFunctionality(self):
		pass
		
class LogOff(UseCase):
	def __init__(self, operatingSystemAPI):
		super().__init__(operatingSystemAPI)
	
	def someFunctionality(self):
		print("Log Off : {}".format(self.operatingSystemAPI.anotherFunctionality()))
	
	
class Shutdown(UseCase):
	def __init__(self, operatingSystemAPI):
		super().__init__(operatingSystemAPI)
	
	def someFunctionality(self):
		print("Shutdown : {}".format(self.operatingSystemAPI.anotherFunctionality()))

		
class OperatingSystemAPI(object):
	def anotherFunctionality(self):
		pass
		
		
class Windows(OperatingSystemAPI):
	def anotherFunctionality(self):
		return "Windows"
		
class Ubuntu(OperatingSystemAPI):
	def anotherFunctionality(self):
		return "Ubuntu"
		
		
def main():
    ubuntu = Ubuntu()
    windows = Windows()

    useCase = LogOff(ubuntu)
    # [1]
    useCase.someFunctionality()
    """
    Output for code [1]
    LogOff : Ubuntu
    """

    useCase = LogOff(windows)
    # [2]
    useCase.someFunctionality()
    """
    Output for code [2]
    LogOff : Windows
    """

    useCase = Shutdown(ubuntu)
    # [3]
    useCase.someFunctionality()
    """
    Output for code [3]
    Shutdown : Ubuntu
    """
    useCase = Shutdown(windows)

    # [4]
    useCase.someFunctionality()
    """
    Output for code [4] :
    Shutdown : Windows
    """


if __name__ == "__main__":
    main()
