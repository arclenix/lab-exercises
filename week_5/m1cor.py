class ProcessHandler(object):

	def __init__(self, successor=None, name=None):
		self._name = name
		self._successor = successor

	def has_successor(self):
		return self._successor is not None

	def process(self):
		pass

	def give_process(self, data):
		self._successor.process(data)


class ProcessLevel1(ProcessHandler):

	def __init__(self, s, n):
		super(ProcessLevel1, self).__init__(s, n)

	def process(self, data):
        # TODO Implement me!
		if data >= 0 and data <= 19:
			print("Using Process Level 1\nProcessing data {}".format(data))
		else:
			self.give_process(data)


class ProcessLevel2(ProcessHandler):

	def __init__(self, s, n):
        # TODO Implement me!
		super(ProcessLevel2, self).__init__(s, n)

	def process(self, data):
        # TODO Implement me!
		if data >= 20 and data <= 39:
			print("Using Process Level 2\nProcessing data {}".format(data))
		else:
			self.give_process(data)


class ProcessLevel3(ProcessHandler):

	def __init__(self, s, n):
        # TODO Implement me!
		super(ProcessLevel3, self).__init__(s, n)

	def process(self, data):
        # TODO Implement me!
		if data >= 40 and data <= 59:
			print("Using Process Level 3\nProcessing data {}".format(data))
		else:
			print("This data can't be processed")

def main():
    p3 = ProcessLevel3(s=None, n='Process Level 3')
    p2 = ProcessLevel2(s=p3, n='Process Level 2')
    p1 = ProcessLevel1(s=p2, n='Process Level 1')
    p1.process(70)  #


if __name__ == "__main__":
    main()
