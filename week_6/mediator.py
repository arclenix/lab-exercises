import random
import time


class TestController:

	def __init__(self):
		self._testmanager = None
		self._bProblem = 0

	def setup(self):
		print("Setting up the Test")
		time.sleep(0.1)
		self._testmanager.prepareReporting()

	def execute(self):
		# TODO COMPLETE ME
		if self._bProblem == 1:
			print("Executing the Test")
			time.sleep(0.1)
		else:
			print("Problem in the setup. Test not executed")

	def tearDown(self):
		# TODO COMPLETE ME
		if self._bProblem == 1:
			print("Tearing Down the Test")
			time.sleep(0.1)
			self._testmanager.publishReport()
		else:
			print("Test not executed. No tear down required.")

	def setTM(self, testmanager):
		# TODO COMPLETE ME
		self._testmanager = testmanager

	def setProblem(self, value):
		# TODO COMPLETE ME
		self._bProblem = value


class Reporter:

	def __init__(self):
		self._testmanager = None

	def prepare(self):
		print("Reporter Class is preparing to report the results")
		time.sleep(0.1)

	def report(self):
		print("Reporting the results of Test")
		time.sleep(0.1)

	def setTM(self, testmanager):
		self._testmanager = testmanager


class Database:

	def __init__(self):
		self._testmanager = None

	def insert(self):
		print("Inserting the execution begin status in the Database ")
		time.sleep(0.1)
		if random.randrange(1, 4) == 3:
			return -1

	def update(self):
		print("Updating the test results in Database")
		time.sleep(0.1)

	def setTM(self, testmanager):
		self._testmanager = testmanager


class TestManager:

	def __init__(self):
		# TODO Complete Me
		self.reporter = None
		self.database = None
		self.testController = None

	def prepareReporting(self):
		# TODO Complete Me
		rvalue = self.database.insert()
		if rvalue == -1:
			self.testController.setProblem(1)
			self.reporter.prepare()

	def setReporter(self, reporter):
		# TODO Complete Me
		self.reporter = reporter

	def setDB(self, database):
		# TODO Complete Me
		self.database = database

	def publishReport(self):
		# TODO Complete Me
		self.database.update()
		self.reporter.report()

	def setTC(self, testcontroller):
		# TODO Complete Me
		self.testController = testcontroller


if __name__ == '__main__':
	reporter = Reporter()
	database = Database()
	testmanager = TestManager()
	testmanager.setReporter(reporter)
	testmanager.setDB(database)
	reporter.setTM(testmanager)
	database.setTM(testmanager)

	for i in range(3):
		testcontroller = TestController()
		testcontroller.setTM(testmanager)
		testmanager.setTC(testcontroller)
		testcontroller.setup()
		testcontroller.execute()
		testcontroller.tearDown()
